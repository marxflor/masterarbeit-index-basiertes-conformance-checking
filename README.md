# Masterarbeit "Index-basiertes Conformance Checking"


This repository contains the implementations and experiment data used in the masters thesis "Index-based Conformance Checking" / "Index-basiertes Conformance Checking".

## Abstract

In the age of digitalisation, the volumes of data and process models collected are becoming ever more extensive and complex. Process mining deals with the analysis and optimisation of those. This paper deals with conformance checking, a part of process mining. This involves comparing expected and actual processes and identifying deviations between them. Alignment approaches are used as standard for this purpose, but they tend to be impractival due to their long runtime.

The focus of this master's thesis is concept development, implementation and evaluation of an index-based approach for conformance checking using an Interleaved Bloom Filter. This should reduce the runtime. The goal is to test whether this approach can be used as an alternative to existing algorithms. For this purpose, experiments are carried out on real data sets and the determined runtimes of the implementation are compared with the state-of-the-art A* algorithm.

After analysing the results, it can be determined that conformance checking can be used with the index-based approach. When searching for optimal alignments with a distance of $0$, better runtimes were achieved compared to the A* algorithm. However, when a degree of error is taken into account, the values determined fluctuate and the average runtime of the implementation is higher than that of the A* algorithm. In conclusion, the implemented index-based approach without error tolerance is an alternative to the comparison algorithm. This is no longer the case if there is a degree of error. Further root cause analysis and evaluation is required in this case.

## File Structure

- [build](https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking/-/tree/main/build): contains build script files
	- "alignment_txt"
	- "txt_to_xes"
	- "xes_to_txt"
- [data](https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking/-/tree/main/data): includes all used experiment data for tests in thesis
- [eclipse](https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking/-/tree/main/eclipse): contains the eclipse project with the A* ProM Plug-in
- [experiment](https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking/-/tree/main/experiment): includes all experiment data and jupyter notebook for evaluations
- [source](https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking/-/tree/main/source): contains implementation of 
- seqan3: links to a subrepository to [SeqAn3](https://github.com/seqan/seqan3)
- [thesis](https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking/-/tree/main/thesis): includes the masters thesis

## Requirements


| | version |
|-------------|---------|
| [GCC](https://gcc.gnu.org)         | ≥ 11   |
| [CMake](https://cmake.org)     | ≥ 3.5       |
| [cereal](https://github.com/USCiLab/cereal)     | ≥ 1.3.1       |


## Run it yourself

```
git clone https://gitlab.informatik.hu-berlin.de/marxflor/masterarbeit-index-basiertes-conformance-checking
cd masterarbeit-index-basiertes-conformance-checking/build
cmake -DCMAKE_BUILD_TYPE=Release ../source
make

./alignment_txt --help
```
