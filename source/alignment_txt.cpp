// SeqAn3 includes
#include <seqan3/argument_parser/all.hpp>
#include <seqan3/core/debug_stream.hpp>
#include <seqan3/search/dream_index/interleaved_bloom_filter.hpp>
#include <seqan3/alphabet/nucleotide/dna4.hpp>
#include <seqan3/search/views/kmer_hash.hpp>

// includes for serialization / deserialization seqan3
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>

// includes for reading traces from log file
#include "pugixml.hpp"
#include <iostream>

// include for time measurement
#include <chrono>

// reading from a text file
#include <fstream>

// include MurMurHash3 hashing algorithm
#include "MurmurHash3.h"


// enum for differentiate between different program executions
enum class Execution_Types { SERIALIZE, DESERIALIZE, NORMAL };


/**
 * @brief Serialize ibf into a file
 * 
 * @param os Output stream of file
 * @param ibf Interleaved Bloom Filter object
 */
void serialize(std::ofstream & os, seqan3::interleaved_bloom_filter<> & ibf)
{
    cereal::BinaryOutputArchive archive(os);
    archive(ibf);
}

/**
 * @brief Deserialize ibf from file
 * 
 * @param is Input stream of file
 * @param ibf Interleaved Bloom Filter object
 */
void deserialize(std::ifstream & is, seqan3::interleaved_bloom_filter<> & ibf)
{
    cereal::BinaryInputArchive archive(is);
    archive(ibf);
}

/**
 * @brief Build all k-mers from given event trace
 * 
 * @param trace_events List of all events in given trace
 * @param k Length of k-mer
 * @param delimiter Delimiter between names of events in kmer
 * @return std::vector<std::string> 
 */
std::vector<std::string> build_kmers(std::vector<std::string> trace_events, int k, std::string delimiter)
{
    // list of resulting k-mers
    std::vector<std::string> kmers;

    // return smaller k-mer if less than k events in trace
    if (trace_events.size() < k)
    {
        // build one smaller k-mer containing all events in trace
        std::string kmer_string;
        for (int j = 0; j < trace_events.size(); j++)
        {
            kmer_string += trace_events[j] + delimiter;
        }
        kmer_string = kmer_string.substr(0, kmer_string.size() - delimiter.size());
        kmers.push_back(kmer_string);
        return kmers;
    }

    // list containing already generated k-mers
    // do not need to be returned twice
    std::set<std::string> already_known_kmers;

    for (int i = 0; i < trace_events.size() - (k - 1); i++)
    {
        // build k-mer from i to (i + (k - 1)) with delimiter
        std::string kmer_string;
        for (int j = 0; j < k; j++)
        {
            kmer_string += trace_events[i + j] + delimiter;
        }
        kmer_string = kmer_string.substr(0, kmer_string.size() - delimiter.size());

        // if k-mer was already generated, skip
        if (already_known_kmers.find(kmer_string) != already_known_kmers.end()) continue;
        already_known_kmers.insert(kmer_string);

        kmers.push_back(kmer_string);
    }

    // return all generated k-mers for given trace and k
    return kmers;
}

/**
 * @brief Calculate levenshtein distance between two words
 * 
 * @param trace1 Trace of events
 * @param trace2 Trace of events
 * @return unsigned int Levenshtein distance between two given words
 */
unsigned int levenshtein_distance(const std::string& trace1, const std::string& trace2)
{
    const std::size_t len1 = trace1.size(), len2 = trace2.size();
    std::vector<std::vector<unsigned int>> d(len1 + 1, std::vector<unsigned int>(len2 + 1));

    d[0][0] = 0;
    for(unsigned int i = 1; i <= len1; ++i) d[i][0] = i;
    for(unsigned int i = 1; i <= len2; ++i) d[0][i] = i;

    for(unsigned int i = 1; i <= len1; ++i)
    {
        for(unsigned int j = 1; j <= len2; ++j)
        {
            d[i][j] = std::min({ d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + (trace1[i - 1] == trace2[j - 1] ? 0 : 1) });
        }
    }

    return d[len1][len2];
}

/**
 * @brief Compare two traces with levenshtein distance
 * 
 * @param trace1 Trace of events
 * @param trace2 Trace of events
 * @return int levenshtein distance between two given words
 */
int compare_traces(std::vector<std::string> trace1, std::vector<std::string> trace2)
{
    std::string trace1_str, trace2_str;
    
    // generate string from traces
    for (const auto &piece : trace1) trace1_str += piece;
    for (const auto &piece : trace2) trace2_str += piece;

    // return levenshtein distance
    return levenshtein_distance(trace1_str, trace2_str);
}


/**
 * @brief Build Interleaved Bloom Filter and fill with traces / deserialize
 * 
 * @param ibf Interleaved Bloom Filter
 * @param file_traces File with given traces
 * @param file_ibf_load_path Path to file to deserialize ibf from
 * @param read_traces List with all read traces
 * @param program_type Execution type of program
 * @param k Length of k-mers
 * @param seed Seed for MurMurHash3
 * @param kmer_count_per_bin Amount of k-mers represented by each bin
 * @param verbose Show / hide verbose message
 * @return true Execution was successful
 * @return false Execution was not successful (error)
 */
bool build_interleaved_bloom_filter(auto & ibf, std::ifstream & file_traces, const char* file_ibf_load_path, std::vector<std::string> & read_traces,
    Execution_Types program_type, int k, int seed, std::vector<int> & kmer_count_per_bin, bool verbose)
{
    // deserialize Interleaved Bloom Filter from file if DESERIALIZE program execution
    if (program_type == Execution_Types::DESERIALIZE)
    {
        std::cout << "Deserializing IBF from file." << std::endl;
        std::ifstream in_file (file_ibf_load_path, std::ios::binary);
        deserialize(in_file, ibf);
        std::cout << "Deserializing IBF complete!" << std::endl;
    }

    if (file_traces.is_open())
    {
        long unsigned int i = 1;

        std::string str;
        while (std::getline(file_traces, str))
        {
            // build vector containing events
            std::vector<std::string> trace_events;

            // delimiter for event separation
            std::string delimiter = " - ";

            // extract events from file line
            std::string trace = str.c_str();
            size_t pos = 0;
            std::string token;
            while ((pos = str.find(delimiter)) != std::string::npos)
            {
                token = str.substr(0, pos);
                trace_events.push_back(token.c_str());
                str.erase(0, pos + delimiter.length());
            }
            trace_events.push_back(str.c_str());

            // build kmers from list of events
            std::vector<std::string> kmers = build_kmers(trace_events, k, delimiter);

            if (kmers.size() > 0)
            {
                read_traces.push_back(trace);

                if (verbose) std::cout << "IBF generation:" << trace << std::endl;

                if (program_type != Execution_Types::DESERIALIZE)
                {
                    if (verbose) std::cout << "- " << trace << std::endl;

                    // hash all k-mers and add into IBF bin
                    for (int j = 0; j < kmers.size(); j++)
                    {
                        uint64_t hash[2];
                        MurmurHash3_x64_128(kmers[j].c_str(), (uint64_t)strlen(kmers[j].c_str()), seed, hash);

                        if (verbose) std::cout << "    k-mer hash: " << ((uint64_t*)hash)[0] << std::endl;

                        ibf.emplace(((uint64_t*)hash)[0], seqan3::bin_index{i - 1});
                    }
                }
                
                // save amount of k-mers represented by this bin
                kmer_count_per_bin.push_back(kmers.size());
            }

            i++;
        }
        return true;
    }
    else
    {
        std::cout << "[Error] Something went wrong while reading traces file!" << std::endl;
        return false;
    }
}

/**
 * @brief Search for traces in Interleaved Bloom Filter
 * 
 * @param ibf Interleaved Bloom Filter
 * @param file_search File with traces to search
 * @param file_output_path Path to file to save results
 * @param read_traces List with all read traces
 * @param search_traces List with all searched traces
 * @param duplicate_detection Enable / disable duplicate detection
 * @param k Length of k-mers
 * @param seed Seed for MurMurHash3
 * @param error Error
 * @param kmer_count_per_bin Amount of k-mers represented by each bin
 * @param verbose Show / hide verbose message
 * @return true Execution was successful
 * @return false Execution was not successful (error)
 */
bool search_in_interleaved_bloom_filter(auto & ibf, std::ifstream & file_search, const char* file_output_path, std::vector<std::string> read_traces, std::vector<std::string> & search_traces,
    bool duplicate_detection, int k, int seed, int error, std::vector<int> & kmer_count_per_bin, bool verbose)
{
    if (file_search.is_open())
    {
        long unsigned int i = 0;

        // list with all already seen traces for duplicate detection
        std::map<std::string, std::string> already_searched_traces;

        // output file
        std::ofstream output_csv (file_output_path);

        if (!output_csv.is_open())
        {
            std::cout << "[Error] Could not create output file!" << std::endl;
            return false;
        }

        // print headers to csv file
        output_csv << "given trace;found trace;levenshtein distance;calculation time [ms];amount found bins\n";

        if (verbose) std::cout << "IBF search:" << std::endl;

        std::string str;
        while (std::getline(file_search, str))
        {
            // start time measurement for each trace
            std::chrono::steady_clock::time_point begin_search = std::chrono::steady_clock::now();

            search_traces.push_back(str.c_str());

            std::string trace = str.c_str();

            if (verbose) std::cout << "- " << trace << std::endl;

            if (duplicate_detection)
            {
                // duplicate detection if enabled
                if (already_searched_traces.find(trace) != already_searched_traces.end())
                {
                    if (verbose) std::cout << "    duplicate!" << std::endl;
                    std::chrono::steady_clock::time_point end_search = std::chrono::steady_clock::now();
                    output_csv << "\"" << trace << "\";" << already_searched_traces[trace] << ";\"" << (std::chrono::duration_cast<std::chrono::microseconds>(end_search - begin_search).count()) / 1000.0 << "\"\n";
                    continue;
                }
            }

            // build vector containing events
            std::vector<std::string> trace_events;
            
            // delimiter for event separation
            std::string delimiter = " - ";

            // extract events from file line
            size_t pos = 0;
            std::string token;
            while ((pos = str.find(delimiter)) != std::string::npos)
            {
                token = str.substr(0, pos);
                trace_events.push_back(token.c_str());
                str.erase(0, pos + delimiter.length());
            }
            trace_events.push_back(str.c_str());

            // build kmers from list of events
            std::vector<std::string> kmers = build_kmers(trace_events, k, delimiter);

            // calculate from each k-mer
            std::vector<uint64_t> hash_values;
            for (int i = 0; i < kmers.size(); i++)
            {
                // hash using MurMurhash3              
                uint64_t hash[2];
                MurmurHash3_x64_128(kmers[i].c_str(), (uint64_t)strlen(kmers[i].c_str()), seed, hash);

                if (verbose) std::cout << "    k-mer hash: " << ((uint64_t*)hash)[0] << std::endl;

                // add values to hash lookup vector
                hash_values.push_back(((uint64_t*)hash)[0]);
            }

            // search for hash matches in IBF
            auto agent = ibf.counting_agent();
            seqan3::counting_vector<short unsigned int> results = agent.bulk_count(hash_values);
            
            if (verbose)
            {
                seqan3::debug_stream << "    matches: " << results << std::endl;
            }

            // get all bins which match in interval
            std::vector<int> matching_bins;
            for (int i = 0; i < results.size(); i++)
            {
                // if matched bin count is in interval
                if (
                    (k * error > kmers.size() || results[i] >= kmers.size() - (k * error))
                    &&
                    kmer_count_per_bin[i] <= results[i] + (k * error)
                )
                {
                    // put bin to list of possible optimal alignments
                    matching_bins.push_back(i);
                }
            }

            int all_matches_count = matching_bins.size();

            // if match was found
            if(all_matches_count > 0)
            {
                // check all possible bins for true positive and choose element with lowest levenshtein distance (alignment)
                int bin_index = 0;

                int closest_alignment_bin = -1;
                int closest_alignment_count = 10000;

                while (bin_index < matching_bins.size())
                {
                    // calculate levenshtein distance
                    int edits = levenshtein_distance(trace, read_traces[matching_bins[bin_index]]);

                    if (edits == 0)
                    {
                        // found exact alignment -> skip others
                        closest_alignment_bin = bin_index;
                        closest_alignment_count = 0;
                        break;
                    }

                    // if distance is smaller, save it as lowest
                    if (edits < closest_alignment_count)
                    {
                        closest_alignment_bin = bin_index;
                        closest_alignment_count = edits;
                    }

                    bin_index++;
                }

                // get best aligned trace from list of read traces
                std::string best_trace = read_traces[matching_bins[closest_alignment_bin]];

                std::chrono::steady_clock::time_point end_search = std::chrono::steady_clock::now();

                // write results to output csv
                output_csv << "\"" << trace << "\";\"" << best_trace << "\";\"" << closest_alignment_count << "\";" << (std::chrono::duration_cast<std::chrono::microseconds>(end_search - begin_search).count()) / 1000.0 << ";" << all_matches_count << "\n";

                // update already searched traces list
                already_searched_traces[trace] = "\"" + best_trace + "\";\"" + std::to_string(closest_alignment_count) + "\"";
            }
            else
            {
                std::chrono::steady_clock::time_point end_search = std::chrono::steady_clock::now();

                // no match found
                output_csv << "\"" << trace << "\";;\"-1\";" << (std::chrono::duration_cast<std::chrono::microseconds>(end_search - begin_search).count()) / 1000.0 << ";" << all_matches_count << "\n";
            }

            i++;
        }
        return true;
    }
    else
    {
        std::cout << "Something went wrong while reading search file!" << std::endl;
        return false;
    }
}


/**
 * @brief Build and search for alignments using an Interleaved Bloom Filter
 * 
 * @param file_traces_path Path to file with traces from model
 * @param file_search_path Path to file with traces from event log
 * @param file_output_path Path to file to save results
 * @param file_ibf_load_path Path to file to deserialize ibf from
 * @param file_ibf_save_path Path to file to serialize ibf to
 * @param program_type Execution type of program
 * @param k Length of k-mers
 * @param error Error
 * @param seed Seed for MurMurHash3
 * @param duplicate_detection Enable / Disable duplicate detection
 */
void search_alignments(const char* file_traces_path, const char* file_search_path, const char* file_output_path,
    const char* file_ibf_load_path, const char* file_ibf_save_path,
    Execution_Types program_type, int k, int error, int seed, bool duplicate_detection, bool verbose)
{
    std::ifstream file_search(file_search_path);
    
    // get amount of traces from file_traces file
    std::ifstream file_traces(file_traces_path);
    long unsigned int traces_count = std::count(std::istreambuf_iterator<char>(file_traces), 
                                                std::istreambuf_iterator<char>(), '\n');
    file_traces.clear();
    file_traces.seekg(0);

    // amount of k-mers represented in each bin
    std::vector<int> kmer_count_per_bin;
    std::vector<std::string> search_traces, read_traces;

    // build Interleaved Bloom Filter - deserialize from file or build from trace file
    std::chrono::steady_clock::time_point begin_ibf_generation = std::chrono::steady_clock::now();
    seqan3::interleaved_bloom_filter ibf{seqan3::bin_count{traces_count},
                                         seqan3::bin_size{2048u},
                                         seqan3::hash_function_count{2u}};
    std::cout << "Building IBF..." << std::endl;
    if (!build_interleaved_bloom_filter(ibf, file_traces, file_ibf_load_path, read_traces, program_type, k, seed, kmer_count_per_bin, verbose)) return;
    std::chrono::steady_clock::time_point end_ibf_generation = std::chrono::steady_clock::now();

    std::cout << "IBF generation finished in " << (std::chrono::duration_cast<std::chrono::microseconds>(end_ibf_generation - begin_ibf_generation).count()) / 1000.0 << "[ms]" << std::endl;

    // serialize Interleaved Bloom Filter if SERIALIZE program execution
    if (program_type == Execution_Types::SERIALIZE)
    {
        std::cout << "Serializing IBF to file." << std::endl;
        std::ofstream out_file (file_ibf_save_path, std::ios::binary);
        serialize(out_file, ibf);
        std::cout << "Serializing IBF complete!" << std::endl;
        return;
    }

    std::chrono::steady_clock::time_point begin_ibf_search = std::chrono::steady_clock::now();

    // search for given traces in Interleaved Bloom Filter
    std::cout << "Search in IBF..." << std::endl;
    if (!search_in_interleaved_bloom_filter(ibf, file_search, file_output_path, read_traces, search_traces, duplicate_detection, k, seed, error,
        kmer_count_per_bin, verbose)) return;
    
    
    std::chrono::steady_clock::time_point end_ibf_search = std::chrono::steady_clock::now();

    std::cout << "IBF search finished in " << (std::chrono::duration_cast<std::chrono::microseconds>(end_ibf_search - begin_ibf_search).count()) / 1000.0 << "[ms]" << std::endl;
    std::cout << "Results saved to " << file_output_path << std::endl;
}

/**
 * @brief Initialize SeqAn3 argument parser
 * 
 * @param parser SeqAn3 argument parser
 */
void initialize_argument_parser(seqan3::argument_parser & parser)
{
    parser.info.author = "Florian Marx";
    parser.info.short_description = "Check alignment of model traces with real time trace logs.";
    parser.info.version = "1.0.0";
}

/**
 * @brief Main function
 * 
 * @param argc Argument count
 * @param argv Argument values
 * @return int Execution code
 */
int main(int argc, char* argv[])
{
    // TODO --verbose

    seqan3::argument_parser argument_parser{"alignment_txt", argc, argv};

    initialize_argument_parser(argument_parser);

    // empty file object to check if files are given in argument parser
    std::filesystem::path empty_file{};
 
    // k-mer length
    int k = 3;
    argument_parser.add_option(k, 'k', "kmer", "The amount of events per mere.");

    // error
    int error = 0;
    argument_parser.add_option(error, 'e', "error", "The maximum allowed error. (must be greater equals 0)");

    // seed for MurMurHash3
    int seed = 42;
    argument_parser.add_option(seed, 'x', "seed", "Set a custom seed for the hash functions.");

    // traces file
    std::filesystem::path traces_file{};
    argument_parser.add_option(traces_file, 't', "traces", "The input TXT file containing traces to search from.",
                               seqan3::option_spec::standard,
                               seqan3::input_file_validator{{"txt"}});
    
    // search file
    std::filesystem::path search_file{};
    argument_parser.add_option(search_file, 's', "search", "The input TXT file containing traces to search in.",
                               seqan3::option_spec::standard,
                               seqan3::input_file_validator{{"txt"}});

    // save ibf file
    std::filesystem::path ibf_file_save{};
    argument_parser.add_option(ibf_file_save, 'd', "dump", "This will only generate the IBF data structure and save it to the given file.",
                               seqan3::option_spec::standard,
                               seqan3::output_file_validator{seqan3::output_file_open_options::open_or_create, {"ibf"}});
    
    // load ibf file
    std::filesystem::path ibf_file_load{};
    argument_parser.add_option(ibf_file_load, 'l', "load", "This will only check conformance with the given IBF data structure.",
                               seqan3::option_spec::standard,
                               seqan3::input_file_validator{{"ibf"}});

    // output file
    std::filesystem::path output_file{};
    argument_parser.add_option(output_file, 'o', "output", "The output CSV file containing all calculated alignment of traces (Will override existing files!).",
                               seqan3::option_spec::standard,
                               seqan3::output_file_validator{seqan3::output_file_open_options::open_or_create, {"csv"}});

    // delimiter for k-mer splitting
    std::string delimiter = "--<DELIM>--";
    argument_parser.add_option(delimiter, 'a', "delimiter", "Set delimiter for k-mer generation");
    
    // show verbose messages
    bool verbose = false;
    argument_parser.add_flag(verbose, 'v', "verbose", "Enable verbose messages for generation and search");

    // enable duplicate detection
    bool duplicate_detection = false;
    argument_parser.add_flag(duplicate_detection, 'z', "duplicate_detection", "Enable duplicate detection");

    try
    {
        argument_parser.parse();

        // error cannot be negative
        if (error < 0)
        {
            std::cout << "[Error] Error argument must be >= 0";
        }
        
        Execution_Types program_type; 

        // check program execution requirements
        if (traces_file != empty_file && ibf_file_save != empty_file)
        {
            // generate ibf and save to file
            program_type = Execution_Types::SERIALIZE;
        }
        else if (search_file != empty_file && ibf_file_load != empty_file && output_file != empty_file)
        {
            // load ibf from file and search traces
            program_type = Execution_Types::DESERIALIZE;
        }
        else if (traces_file != empty_file && search_file != empty_file && output_file != empty_file)
        {
            // generate ibf and search traces
            program_type = Execution_Types::NORMAL;
        }

        search_alignments(
            traces_file.string().c_str(), search_file.string().c_str(), output_file.string().c_str(),
            ibf_file_load.string().c_str(), ibf_file_save.string().c_str(),
            program_type, k, error, seed, duplicate_detection, verbose);
    }
    catch (seqan3::argument_parser_error const & ext)
    {
        seqan3::debug_stream << "[Error] " << ext.what() << "\n";
        return -1;
    }

    return 0;
}