package org.processmining.plugins.testing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.alignment.plugin.AbstractAlignmentPlugin;
import org.processmining.alignment.plugin.IterativeAStarPlugin;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.log.utils.XLogBuilder;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetGraph;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.plugins.petrinet.replayer.algorithms.IPNReplayParameter;
import org.processmining.plugins.petrinet.replayer.algorithms.costbasedcomplete.CostBasedCompleteParam;

import nl.tue.alignment.ReplayerParameters;
import nl.tue.alignment.algorithms.ReplayAlgorithm.Debug;
import nl.tue.astar.AStarException;

public class AStarTimings {
	// IMPORTANT: To use this Plug-in you have to edit this path to output file
	static String OUTPUT_FILE_PATH = "C:\\Users\\ohne_\\Desktop\\masterthesis\\AStar_Timings.txt";
	
	@Plugin(
		name = "A* Time Measurement",
		parameterLabels = { "Petri Net", "XLog", "Initial Marking", "Final Marking" }, 
		returnLabels = { "Time measurement" }, 
		returnTypes = { String.class }, 
		userAccessible = true, 
		help = "Measures time of A* execution"
	)
	@UITopiaVariant(
		affiliation = "HU Berlin", 
		author = "Florian Marx", 
		email = "marxflor@informatik.hu-berlin.de"
	)
	public static String astarTimings(PluginContext context, Petrinet petrinet, XLog xlog, Marking initialMarking, Marking finalMarking) {	
		FileWriter outputFile;
		try {
			outputFile = new FileWriter(OUTPUT_FILE_PATH);
		} catch (IOException e) {
			e.printStackTrace();
			return "Some error occurred!";
		}
		
		// get a* alignment
		AbstractAlignmentPlugin alignmentPlugin = new IterativeAStarPlugin();
		
		// build replayer parameters
		ReplayerParameters replayParameters = new ReplayerParameters.IncrementalAStar(false, 1, false,
				Debug.NONE, Integer.MAX_VALUE, 2000, Integer.MAX_VALUE, false, false);
		
		XEventClass dummyEvClass = new XEventClass("DUMMY", 99999);
		XEventClassifier eventClassifier = XLogInfoImpl.STANDARD_CLASSIFIER;
		TransEvClassMapping mapping = constructMapping(petrinet, xlog, dummyEvClass, eventClassifier);
		
		Map<Transition, Integer> transitions2costs = constructTTCMap(petrinet);
        Map<XEventClass, Integer> events2costs = constructETCMap(petrinet, eventClassifier, xlog, dummyEvClass);
        
        IPNReplayParameter parameters = constructParameters(transitions2costs, events2costs, petrinet, initialMarking, finalMarking);
        
        XLog[] xlogs = new XLog[xlog.size()];
        for (int i = 0; i < xlog.size(); i++) {
        	XLogBuilder logBuilder = XLogBuilder.newInstance()
        			.startLog("log")
        			.addTrace("trace" + i);
        	
        	XTrace trace = xlog.get(i);
        	for (int j = 0; j < trace.size(); j++) {
        		logBuilder.addEvent(trace.get(j).getAttributes().get("concept:name").toString());
        	}
        	
        	xlogs[i] = logBuilder.build();
        }
        
		try {
			BufferedWriter writer = new BufferedWriter(outputFile);
			
			writer.write("given trace;calculation time [ms]\n");
			
			for (int i = 0; i < xlogs.length; i++) {
				long startTime = System.nanoTime();
				// play log with a*
				alignmentPlugin.replayLog(context, petrinet, xlogs[i], mapping, parameters);
				long endTime = System.nanoTime();
				
				// get log events
				String tracelog = "";
				for (int j = 0; j < xlogs[i].get(0).size(); j++) {
					tracelog += xlogs[i].get(0).get(j).getAttributes().get("concept:name") + " - ";
				}
				
				tracelog = tracelog.substring(0, tracelog.length() - 3);
				
				// write results to output file
				writer.write("\"" + tracelog + "\";" + (endTime/1000 - startTime/1000)/1000f + "\n");
			}
			
			writer.close();
			return ("Finished!");
		} catch (AStarException e) {
			e.printStackTrace();
			return "Some error occurred!";
		} catch (IOException e) {
			e.printStackTrace();
			return "Some error occurred!";
		}
	}
	
	private static Map<Transition, Integer> constructTTCMap(Petrinet petrinet) {
		Map<Transition, Integer> transitions2costs = new HashMap<Transition, Integer>();

        for(Transition t : petrinet.getTransitions()) {
            if(t.isInvisible()) {
                transitions2costs.put(t, 0);
            }else {
                transitions2costs.put(t, 1);
            }
        }
        return transitions2costs;
    }

    private static Map<XEventClass, Integer> constructETCMap(Petrinet petrinet, XEventClassifier xEventClassifier, XLog log, XEventClass dummyEvClass) {
    	Map<XEventClass, Integer> costMOT = new HashMap<XEventClass, Integer>();
        XLogInfo summary = XLogInfoFactory.createLogInfo(log, xEventClassifier);

        for (XEventClass evClass : summary.getEventClasses().getClasses()) {
            int value = 1;
            for(Transition t : petrinet.getTransitions()) {
                if(t.getLabel().equals(evClass.getId())) {
                    value = 1;
                    break;
                }
            }
            costMOT.put(evClass, value);
        }

        costMOT.put(dummyEvClass, 1);

        return costMOT;
    }

	private static IPNReplayParameter constructParameters(Map<Transition, Integer> transitions2costs, Map<XEventClass, Integer> events2costs, Petrinet petrinet, Marking initialMarking, Marking finalMarking) {
        IPNReplayParameter parameters = new CostBasedCompleteParam(events2costs, transitions2costs);
        
        parameters.setInitialMarking(initialMarking);
        parameters.setFinalMarkings(finalMarking);
        parameters.setGUIMode(false);
        parameters.setCreateConn(false);
        ((CostBasedCompleteParam) parameters).setMaxNumOfStates(Integer.MAX_VALUE);

        return  parameters;
    }
	
	private static TransEvClassMapping constructMapping(PetrinetGraph net, XLog log, XEventClass dummyEvClass, XEventClassifier eventClassifier) {
		TransEvClassMapping mapping = new TransEvClassMapping(eventClassifier, dummyEvClass);
		XLogInfo summary = XLogInfoFactory.createLogInfo(log, eventClassifier);
		for (Transition t : net.getTransitions()) {
			boolean mapped = false;
			for (XEventClass evClass : summary.getEventClasses().getClasses()) {
				String id = evClass.getId();
				String label = t.getLabel();
				id = id.substring(0, id.length()-1);
				if (label.equals(id)) {
					mapping.put(t, evClass);
					mapped = true;
					break;
				}
			}
		}
		return mapping;
	}
}
